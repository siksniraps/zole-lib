package lv.id.siksniraps.zolelib.uct

import lv.id.siksniraps.zolelib.moves.Move
import lv.id.siksniraps.zolelib.players.Player

internal class ChildNode(val parent: Node, val move: Move, private val playerId: Int) : Node() {
    fun backpropagation(playerPoints: Map<Player, Int>) {
        value += playerPoints.mapKeys { it.key.id }[playerId] ?: throw IllegalStateException("Node does not belong to any player")
        value += if ((playerPoints.mapKeys { it.key.id }[playerId] ?: throw IllegalStateException("Node does not belong to any player")) > 0) {
            1
        } else {
            0
        }
        if (parent is ChildNode) {
            parent.backpropagation(playerPoints)
        }
    }
}
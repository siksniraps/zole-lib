package lv.id.siksniraps.zolelib.uct

import lv.id.siksniraps.zolelib.game.Zole
import lv.id.siksniraps.zolelib.moves.Move
import lv.id.siksniraps.zolelib.players.Player

internal open class Node {

    var value = 0

    var visits = 1

    val children: Set<ChildNode>
        get() = _children.toSet()

    private val _children: MutableSet<ChildNode> = mutableSetOf()

    private fun addMove(move: Move, player: Player): ChildNode {
        val node = ChildNode(this, move, player.id)
        _children.add(node)
        return node
    }

    fun hasUnexaminedMoves(game: Zole) = game.activePlayer.legalMoves(game).any { it !in _children.map { it.move } }

    fun select(game: Zole): Node {
        val bestNodes = _children.groupBy { ucb(it) }.
                maxBy { it.key }?.value ?: throw IllegalStateException("No moves to select")
        val move = bestNodes[game.rng.nextInt(bestNodes.size)]
        visits++
        return move
    }

    private fun ucb(node: Node): Double {
        return node.value / node.visits + 50 * Math.sqrt(Math.log(visits.toDouble()) / node.visits)
    }

    fun expand(game: Zole): Node {
        val unexaminedMovs = game.activePlayer.legalMoves(game).filter { it !in _children.map { it.move } }
        if (unexaminedMovs.isEmpty()) {
            throw IllegalStateException("No moves to expand")
        }
        visits++
        return addMove(unexaminedMovs[game.rng.nextInt(unexaminedMovs.size)], game.activePlayer)
    }

}
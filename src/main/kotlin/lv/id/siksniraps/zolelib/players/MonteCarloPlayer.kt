package lv.id.siksniraps.zolelib.players

import lv.id.siksniraps.zolelib.game.Zole
import lv.id.siksniraps.zolelib.moves.Move
import kotlin.properties.Delegates

class MonteCarloPlayer(name: String, private val simulationTime: Long) : Player(name) {

    override suspend fun doTurn(game: Zole) {
        val moves: MutableMap<Move, Int> = mutableMapOf()
        val startTime = System.currentTimeMillis()
        do {
            val gameCopy = game.copy {
                when (it) {
                    game.activePlayer -> object : RandomPlayer("${it.name}-simulation") {
                        var move: Move? by Delegates.vetoable(null) { _, oldValue: Move?, newValue: Move? ->
                            oldValue == null && newValue != null
                        }

                        override suspend fun doTurn(game: Zole) {
                            super.doTurn(game)
                        }

                        override suspend fun endTurn(move: Move) {
                            this.move = move
                            super.endTurn(move)
                        }

                        override fun onGameOver(game: Zole) {
                            with(move) {
                                if (this != null) {
                                    moves[this] = score(game) + (moves[this] ?: 0)
                                }
                            }
                            super.onGameOver(game)
                        }

                    }
                    else -> RandomPlayer("${it.name} simulation")
                }
            }
            gameCopy.start()
        } while (startTime + simulationTime > System.currentTimeMillis())
        endTurn(moves.maxBy { it.value }!!.key)
    }
}

package lv.id.siksniraps.zolelib.players

import lv.id.siksniraps.zolelib.game.Zole
import lv.id.siksniraps.zolelib.uct.ChildNode
import lv.id.siksniraps.zolelib.uct.Tree

class MonteCarloTreePlayer(name: String, private val simulationTime: Long) : Player(name) {

    override suspend fun doTurn(game: Zole) {
        endTurn(if (legalMoves(game).size == 1) {
            legalMoves(game)[0]
        } else {
            val tree = Tree()
            val startTime = System.currentTimeMillis()
            do {
                var currentNode = tree.root
                var simulation = false
                val gameCopy = game.copy {
                    object : RandomPlayer("${it.name}-simulation") {

                        override suspend fun doTurn(game: Zole) {
                            if (!simulation) {
                                currentNode = if (currentNode.hasUnexaminedMoves(game)) {
                                    simulation = true
                                    currentNode.expand(game)
                                } else {
                                    currentNode.select(game)
                                }
                                endTurn((currentNode as? ChildNode ?: throw IllegalStateException("Child is not child node")).move)
                            } else {
                                super.doTurn(game)
                            }
                        }

                    }
                }
                gameCopy.start()
                (currentNode as ChildNode).backpropagation(gameCopy.players.associate { it to it.score(gameCopy) })
            } while (startTime + simulationTime > System.currentTimeMillis())
            val bestMoves = tree.root.children.groupBy { it.visits }.maxBy { it.key }?.value ?: throw IllegalStateException("No moves to play")
            bestMoves[game.rng.nextInt(bestMoves.size)].move
        })

    }
}
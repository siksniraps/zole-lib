package lv.id.siksniraps.zolelib.players

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import lv.id.siksniraps.zolelib.common.Card
import lv.id.siksniraps.zolelib.common.Suit
import lv.id.siksniraps.zolelib.game.Phase
import lv.id.siksniraps.zolelib.game.TeamType
import lv.id.siksniraps.zolelib.game.Zole
import lv.id.siksniraps.zolelib.game.ZoleEvents
import lv.id.siksniraps.zolelib.moves.Decision
import lv.id.siksniraps.zolelib.moves.Move
import nl.komponents.kovenant.Deferred
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.deferred

abstract class Player(val name: String) : ZoleEvents {

    var id: Int = -1

    private var deferredMove: Deferred<Move, IllegalStateException>? = null

    private val _cards: MutableList<Card> = mutableListOf()

    val cards: List<Card>
        get() = _cards.toList()

    fun pushCard(vararg cards: Card) {
        _cards.addAll(cards)
    }

    suspend fun startTurn(game: Zole): Promise<Move, IllegalStateException> {
        val deferredMove: Deferred<Move, IllegalStateException> = deferred()
        this.deferredMove = deferredMove
        launch(CommonPool) { doTurn(game) }
        return deferredMove.promise
    }

    protected abstract suspend fun doTurn(game: Zole)

    open suspend fun endTurn(move: Move) {
        this.deferredMove?.resolve(move)
    }

    fun team(game: Zole) = game.playerToTeam[this] ?: throw IllegalStateException("Player does not have a team!")

    fun removeCard(card: Card) = _cards.remove(card)


    fun replaceCards(cards: List<Card>) {
        _cards.clear()
        _cards.addAll(cards)
    }

    fun hasTrumps() = _cards.any { it.trump }

    fun hasSuit(suit: Suit) = _cards.any { !it.trump && it.suit === suit }

    fun score(game: Zole) = with(team(game)) {
        val basePoints = if (this.hasWon(game)) this.type.winBase else this.type.loseBase
        val bonus = when {
            this.tricks.size == Zole.MAX_TRICKS -> 2
            this.points > 90 -> 1
            this.tricks.isEmpty() -> -2
            this.points < 30 -> -1
            else -> 0
        }
        when (type) {
            TeamType.BIG, TeamType.ZOLE -> basePoints + bonus * 2
            TeamType.SMALL, TeamType.ZOLE_OPPONENTS -> basePoints + bonus
            TeamType.SMALL_ZOLE, TeamType.SMALL_ZOLE_OPPONENTS, TeamType.TABLE -> basePoints
        }

    }

    fun legalMoves(game: Zole) = if (game.activePlayer == this) when (game.phase) {
        Phase.DECISION -> Decision.values().toList()
        Phase.BURY, Phase.PLAY -> cards.filter {
            game.canPlayCard(
                    this, it as? Card ?: throw IllegalStateException("Incorrect type of move!")
            )
        }
        else -> emptyList()
    } else emptyList()


}


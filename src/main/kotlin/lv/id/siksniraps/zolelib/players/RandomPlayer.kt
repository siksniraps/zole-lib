package lv.id.siksniraps.zolelib.players

import lv.id.siksniraps.zolelib.game.Zole

open class RandomPlayer(name: String) : Player(name) {
    override suspend fun doTurn(game: Zole) = endTurn(with(legalMoves(game)) { get(game.rng.nextInt(size)) })
}
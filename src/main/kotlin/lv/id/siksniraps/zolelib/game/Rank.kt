package lv.id.siksniraps.zolelib.common


enum class Rank(private val shortName: String) {

    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    SIX("6"),
    SEVEN("7"),
    EIGHT("8"),
    NINE("9"),
    TEN("10"),
    KING("K"),
    JACK("J"),
    QUEEN("Q"),
    ACE("A");

    override fun toString() = shortName

}
package lv.id.siksniraps.zolelib.game

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import lv.id.siksniraps.zolelib.common.Card
import lv.id.siksniraps.zolelib.common.Deck
import lv.id.siksniraps.zolelib.moves.Decision
import lv.id.siksniraps.zolelib.moves.Move
import lv.id.siksniraps.zolelib.players.Player
import java.util.*

private typealias ActionWithCardHandler = (Card, Player) -> Unit
private typealias ActionWithDecisionHandler = (Decision, Player) -> Unit

class Zole private constructor(
        val players: List<Player>,
        val rng: Random
) {

    var phase: Phase = Phase.NEW_GAME
        private set

    private val _teams: MutableList<Team> = mutableListOf()
    val teams: List<Team>
        get() = _teams.toList()

    var activeTrick: Trick = Trick()
        private set

    private var playerTeamLinks: MutableMap<Int, Int> = mutableMapOf()

    private var deck: Deck = Deck()
    private var table: Table = Table()


    private var activePlayerId = rng.nextInt(MAX_PLAYERS)
    private var firstHandId = activePlayerId
    private var decisionsMade = 0
    private var cardsBuried = 0
    var tricksPlayed = 0
        private set

    private val listeners = ZoleListeners()

    fun setListeners(initListeners: ZoleListeners.() -> Unit): Zole {
        listeners.initListeners()
        return this
    }

    private var cardPlayedHandler: MutableList<ActionWithCardHandler> = mutableListOf()
    private var cardBuriedHandler: MutableList<ActionWithCardHandler> = mutableListOf()
    private var decisionMadeHandler: MutableList<ActionWithDecisionHandler> = mutableListOf()

    private fun onCardPlayed(card: Card, player: Player) {
        cardPlayedHandler.forEach { it.invoke(card, player) }
    }

    private fun onCardBuried(card: Card, player: Player) {
        cardBuriedHandler.forEach { it.invoke(card, player) }
    }

    private fun onDecisionMade(decision: Decision, player: Player) {
        decisionMadeHandler.forEach { it.invoke(decision, player) }
    }


    inner class ZoleListeners {
        fun onCardPlayed(handler: ActionWithCardHandler) {
            cardPlayedHandler.add(handler)
        }

        fun onCardBuried(handler: ActionWithCardHandler) {
            cardBuriedHandler.add(handler)
        }

        fun onDecisionMade(handler: ActionWithDecisionHandler) {
            decisionMadeHandler.add(handler)
        }
    }


    companion object {
        const val MAX_PLAYERS = 3
        const val MAX_TRICKS = 8
        const val MAX_CARDS_BURIED = 2
        fun create(seed: Long, players: List<Player>) = Zole(rng = Random(seed), players = players)
    }


    val playerToTeam: Map<Player, Team>
        get() = playerTeamLinks.entries.associate { players[it.key] to teams[it.value] }

    val activePlayer: Player
        get() = players[activePlayerId]

    val doMove: Map<Phase, suspend (Move) -> Unit> = mapOf(
            Phase.DECISION to { decision ->
                makeDecision(decision as? Decision ?: throw IllegalStateException("Wrong type of move!"))
            },
            Phase.BURY to { card ->
                buryCard(card as? Card ?: throw IllegalStateException("Wrong type of move!"))
            },
            Phase.PLAY to { card ->
                playCard(card as? Card ?: throw IllegalStateException("Wrong type of move!"))
            }
    )

    val moveFilters: MutableMap<Player, MutableList<(List<Card>) -> (List<Card>)>> = mutableMapOf() // TODO: move away

    init {
        players.forEachIndexed { index, player ->
            player.id = index
            moveFilters[player] = mutableListOf()
        }
    }


    suspend fun start() {
        while (true) {
            when (phase) {
                Phase.NEW_GAME -> setUpGame()
                Phase.DECISION, Phase.BURY, Phase.PLAY -> {
                    doMove[phase]!!(activePlayer.startTurn(this@Zole).get())
                }
                Phase.GAME_OVER -> {
                    players.map { launch(CommonPool) { it.onGameOver(this@Zole) } }.forEach { it.join() }
                    return
                }
            }

        }
    }

    private suspend fun setUpGame() {
        deck.initializeCards()
        deck.shuffle(rng)
        deal()
        // Wait for players to do their game prep
        players.map { launch(CommonPool) { it.onStartGame(this@Zole) } }.forEach { it.join() }
        phase = Phase.DECISION
        nextTurn(activePlayerId)
    }

    private fun deal() {
        if (!deck.isFull()) {
            throw IllegalStateException("Deck is not full!")
        }
        if (players.count { it.cards.isEmpty() } != MAX_PLAYERS) {
            throw IllegalStateException("Some players already have cards!")
        }
        players.forEach { player ->
            val dealtCards = deck.pop(MAX_TRICKS)
            player.pushCard(*dealtCards.toTypedArray())
        }
        table.push(*deck.pop(Table.MAX_CARDS_ON_TABLE).toTypedArray())
    }

    private suspend fun makeDecision(decision: Decision) {
        decisionsMade++
        players.map { launch(CommonPool) { it.onDecisionMade(this@Zole, activePlayer, decision) } }.forEach { it.join() }
        when (decision) {
            Decision.TAKE -> {

                _teams.add(Team(TeamType.BIG))
                playerTeamLinks[activePlayerId] = teams.size - 1

                _teams.add(Team(TeamType.SMALL))
                (0..2).filterNot { it == activePlayerId }.forEach { playerTeamLinks[it] = teams.size - 1 }

                activePlayer.pushCard(*table.popAll().toTypedArray())
                phase = Phase.BURY
                // The same player is up next because he needs to bury cards
                nextTurn(activePlayerId)
            }
            Decision.ZOLE -> {

                _teams.add(Team(TeamType.ZOLE))
                playerTeamLinks[activePlayerId] = teams.lastIndex

                val zoleOpponents = Team(TeamType.ZOLE_OPPONENTS)
                _teams.add(zoleOpponents)
                players.indices.filterNot { it == activePlayerId }.forEach { playerTeamLinks[it] = teams.lastIndex }

                zoleOpponents.table.push(*table.popAll().toTypedArray())
                phase = Phase.PLAY
                nextTurn(firstHandId)
            }
            Decision.SMALL_ZOLE -> {

                _teams.add(Team(TeamType.SMALL_ZOLE))
                playerTeamLinks[activePlayerId] = teams.lastIndex

                _teams.add(Team(TeamType.SMALL_ZOLE_OPPONENTS))
                players.indices.filterNot { it == activePlayerId }.forEach { playerTeamLinks[it] = teams.lastIndex }

                phase = Phase.PLAY
                nextTurn(firstHandId)
            }
            Decision.PASS -> {
                if (decisionsMade == MAX_PLAYERS) {
                    players.indices.forEach {
                        _teams.add(Team(TeamType.TABLE))
                        playerTeamLinks[it] = teams.lastIndex
                    }
                    phase = Phase.PLAY
                    nextTurn(firstHandId)
                } else {
                    nextTurn()
                }
            }
        }

    }

    private suspend fun nextTurn(nextPlayer: Int = (activePlayerId + 1) % players.size) {
        activePlayerId = nextPlayer
        players.map { launch(CommonPool) { it.onStartTurn(this@Zole, players[activePlayerId]) } }.forEach { it.join() }
    }

    private suspend fun buryCard(card: Card) {
        activePlayer.removeCard(card)
        activePlayer.team(this).table.push(card)
        cardsBuried++
        players.map { launch(CommonPool) { it.onCardBuried(this@Zole, activePlayer) } }.forEach { it.join() }
        if (cardsBuried == MAX_CARDS_BURIED) {
            phase = Phase.PLAY
            nextTurn(firstHandId)
        }
    }

    private suspend fun playCard(card: Card) {
        //TODO: Move into player class. Maybe list of observations?
        with(activeTrick.firstCard) {
            if (this != null) {
                if (!this.trump && this.suit != card.suit) {
                    moveFilters[activePlayer]?.add({ it.filter { it.suit != card.suit } })
                }
                if (this.trump && !card.trump) {
                    moveFilters[activePlayer]?.add({ it.filter { !it.trump } })
                }
            }
        }

        activePlayer.removeCard(card)
        activeTrick.playCard(activePlayerId, card)
        players.map { launch(CommonPool) { it.onCardPlayed(this@Zole, activePlayer, card) } }.forEach { it.join() }

        if (activeTrick.isFull()) {
            tricksPlayed++
            val winningPlayer = players[activeTrick.winnerSeat()]
            val winningTeam = winningPlayer.team(this)
            winningTeam.tricks.add(activeTrick)
            players.map {
                launch(CommonPool) {
                    it.onPlayerTakesTrick(this@Zole, winningPlayer)
                }
            }.forEach { it.join() }
            if (tricksPlayed == MAX_TRICKS || winningTeam.type == TeamType.SMALL_ZOLE) {
                phase = Phase.GAME_OVER
            } else {
                activeTrick = Trick()
                nextTurn(winningPlayer.id)
            }
        } else {
            nextTurn()
        }
    }

    //TODO: this is not really a copy method
    fun copy(playerMapper: (Player) -> Player): Zole {
        val game = Zole(
                players = players.map {
                    val player = playerMapper(it)
                    player.replaceCards(it.cards.map { it.copy() })
                    player.id = it.id
                    player
                },
                rng = rng)
        game.phase = phase
        game._teams.map { it.copy() }.toMutableList()
        game.playerTeamLinks = playerTeamLinks.toMutableMap()
        game.activeTrick = activeTrick.copy()
        game.deck = deck.copy()
        game.table = table.copy()
        game.activePlayerId = activePlayerId
        game.firstHandId = firstHandId
        game.decisionsMade = decisionsMade
        game.cardsBuried = cardsBuried
        game.tricksPlayed = tricksPlayed
        game.scramble()
        return game
    }

    private fun scramble() {
        val unknownCards: MutableList<Card> = mutableListOf()
        val playerCardCount: MutableMap<Player, Int> = mutableMapOf()
        val table = when {
            !this.table.isEmpty() -> this.table
            activePlayer.team(this).type == TeamType.BIG -> teams.first { it != activePlayer.team(this) }.table
            else -> Table()
        }
        unknownCards.addAll(table.popAll())
        players.filterNot { it == activePlayer }.forEach {
            unknownCards.addAll(it.cards.map { it as? Card ?: throw IllegalStateException("Incorrect type of card") })
            playerCardCount[it] = it.cards.size
        }
        val deck = Deck(unknownCards)
        deck.shuffle(rng)
        val cards = deck.pop(deck.size).toMutableList()
        playerCardCount.forEach { player, count ->
            var filteredCards = mutableListOf(*cards.toTypedArray())
            moveFilters[player]?.forEach {
                filteredCards = it(filteredCards).toMutableList()
            }
            val chosen = filteredCards.take(count)
            chosen.forEach { cards.remove(it) }
            player.replaceCards(chosen)
        }
        table.push(*cards.toTypedArray())
    }

    fun canPlayCard(player: Player, card: Card) = activeTrick.canPlayCard(player, card)

}
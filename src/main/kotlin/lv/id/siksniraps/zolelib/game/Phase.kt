package lv.id.siksniraps.zolelib.game

enum class Phase {

    NEW_GAME,
    DECISION,
    BURY,
    PLAY,
    GAME_OVER

}
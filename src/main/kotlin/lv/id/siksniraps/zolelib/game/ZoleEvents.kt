package lv.id.siksniraps.zolelib.game

import lv.id.siksniraps.zolelib.common.Card
import lv.id.siksniraps.zolelib.moves.Decision
import lv.id.siksniraps.zolelib.players.Player

interface ZoleEvents {
    fun onStartGame(game: Zole) {}
    fun onStartTurn(game: Zole, player: Player) {}
    fun onDecisionMade(game: Zole, player: Player, decision: Decision) {}
    fun onCardBuried(game: Zole, player: Player) {}
    fun onCardPlayed(game: Zole, player: Player, card: Card) {}
    fun onPlayerTakesTrick(game: Zole, player: Player) {}
    fun onGameOver(game: Zole) {}
}
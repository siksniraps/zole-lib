package lv.id.siksniraps.zolelib.game

class Team(
        val type: TeamType,
        val tricks: MutableList<Trick> = mutableListOf(),
        val table: Table = Table()
) {

    val points
        get() = tricks.flatMap { it.cards }.sumBy { it.points } + table.cards.sumBy { it.points }

    fun players(game: Zole) = game.playerToTeam.filterValues { it == this }.keys.toList()

    fun hasWon(game: Zole) = when (type) {
        TeamType.BIG, TeamType.ZOLE -> points > 60
        TeamType.SMALL, TeamType.ZOLE_OPPONENTS -> points > 59
        TeamType.SMALL_ZOLE -> tricks.isEmpty() && game.tricksPlayed == Zole.MAX_TRICKS
        TeamType.SMALL_ZOLE_OPPONENTS -> tricks.size < Zole.MAX_TRICKS
        TeamType.TABLE -> game.teams.filterNot { it == this }.any {
            (it.tricks.size > tricks.size || it.tricks.size == tricks.size && it.points > points)
                    && game.tricksPlayed == Zole.MAX_TRICKS
        }
    }

    fun copy(): Team = Team(type, tricks.map { it.copy() }.toMutableList(), table.copy())

}
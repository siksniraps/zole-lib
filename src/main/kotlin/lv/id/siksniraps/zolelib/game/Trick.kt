package lv.id.siksniraps.zolelib.game

import lv.id.siksniraps.zolelib.common.Card
import lv.id.siksniraps.zolelib.players.Player

class Trick {

    var firstCard: Card? = null
        set(value) {
            if (field == null) {
                field = value
            }
        }

    companion object {
        const val MAX_CARDS_IN_TRICK = 3
    }

    private val seatToCard: MutableMap<Int, Card> = mutableMapOf()

    val cards
        get() = seatToCard.values

    fun copy(): Trick {
        val trick = Trick()
        trick.seatToCard.putAll(this.seatToCard.mapValues { it.value.copy() })
        trick.firstCard = this.firstCard
        return trick
    }

    fun playCard(seat: Int, card: Card) {
        if (isFull()) {
            throw IllegalStateException("Too many cards in trick!")
        }
        if (isEmpty()) {
            firstCard = card
        }
        seatToCard[seat] = card
    }

    fun isFull() = seatToCard.size == MAX_CARDS_IN_TRICK

    fun isEmpty() = seatToCard.isEmpty()

    fun winnerSeat() = with(firstCard) {
        if (seatToCard.size != MAX_CARDS_IN_TRICK || this == null) {
            throw IllegalStateException("Trick is not over yet")
        }
        seatToCard.filter { it.value.suit === this.suit || it.value.trump }.maxBy { it.value.strength }!!.key
    }


    fun canPlayCard(player: Player, card: Card) = with(firstCard) {
        when {
            this == null -> true
            this.trump -> card.trump || !player.hasTrumps()
            else -> (!card.trump && card.suit === this.suit) || !player.hasSuit(this.suit)
        }
    }

}


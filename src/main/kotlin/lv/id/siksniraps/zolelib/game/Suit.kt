package lv.id.siksniraps.zolelib.common

enum class Suit(private val shortName: String) {

    DIAMONDS("D"),
    HEARTS("H"),
    SPADES("S"),
    CLUBS("C");

    override fun toString() = shortName

}
package lv.id.siksniraps.zolelib.game

enum class TeamType(val winBase: Int, val loseBase: Int) {
    BIG(2, -4),
    SMALL(2, -1),
    ZOLE(10, -12),
    ZOLE_OPPONENTS(6, -5),
    SMALL_ZOLE(12, -14),
    SMALL_ZOLE_OPPONENTS(7, -6),
    TABLE(2, -4);

}
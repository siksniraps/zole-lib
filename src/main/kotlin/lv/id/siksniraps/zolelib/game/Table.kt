package lv.id.siksniraps.zolelib.game

import lv.id.siksniraps.zolelib.common.Card


class Table(private val _cards: MutableList<Card> = mutableListOf()) {

    companion object {
        const val MAX_CARDS_ON_TABLE = 2
    }

    val cards
        get() = _cards.toList()

    val points
        get() = _cards.sumBy { it.points }

    fun push(vararg cards: Card) {
        if (_cards.size + cards.size > MAX_CARDS_ON_TABLE) {
            throw IllegalStateException("Too many cards on the table!")
        }
        _cards.addAll(cards)
    }

    fun pop(count: Int = 1): List<Card> {
        if (_cards.size < count) {
            throw IllegalStateException("Not enough cards on the table!")
        }
        val popped = _cards.takeLast(count)
        _cards.removeAll(popped)
        return popped
    }

    fun popAll(): List<Card> {
        val popped = _cards.toList()
        _cards.clear()
        return popped
    }

    fun isEmpty() = cards.isEmpty()

    fun copy(): Table = Table(_cards.map { it.copy() }.toMutableList())

}
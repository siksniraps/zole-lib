package lv.id.siksniraps.zolelib.moves

import lv.id.siksniraps.zolelib.game.Card


interface Move {

    fun toCard() = this as? Card ?: throw TypeCastException("Move is not a Card!")

    fun toDecision() = this as? Decision ?: throw TypeCastException("Move is not a Decision!")
}
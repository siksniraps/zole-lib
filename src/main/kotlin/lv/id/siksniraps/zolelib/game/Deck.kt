package lv.id.siksniraps.zolelib.common

import java.util.*


class Deck(val cards: MutableList<Card> = mutableListOf()) : Iterable<Card> {

    companion object {
        const val MAX_CARDS = 26
    }

    fun copy() = Deck(cards.map { it.copy() }.toMutableList())

    fun initializeCards() {
        listOf(
                Rank.SEVEN, Rank.EIGHT, Rank.NINE, Rank.KING,
                Rank.TEN, Rank.ACE, Rank.JACK, Rank.QUEEN
        ).forEach { rank ->
            enumValues<Suit>().forEach { suit ->
                if ((rank != Rank.SEVEN && rank != Rank.EIGHT) || suit == Suit.DIAMONDS) {
                    cards.add(Card(rank, suit))
                }
            }
        }
    }

    fun isFull() = cards.size == MAX_CARDS

    val size
        get() = cards.size

    fun isEmpty() = cards.isEmpty()

    fun pop(count: Int = 1): List<Card> {
        if (cards.size < count) {
            throw IllegalStateException("Not enough cards in deck")
        }
        val popped = cards.takeLast(count)
        cards.removeAll(popped)
        return popped
    }

    fun push(vararg cards: Card) {
        this.cards.addAll(cards)
    }

    // Fisher-Yates-Durstenfeld Shuffle
    fun shuffle(rng: Random) {
        (0..cards.size - 2).forEach {
            val to = rng.nextInt(cards.size - it) + it
            val card = cards[to]
            cards[to] = cards[it]
            cards[it] = card
        }
    }

    operator fun get(i: Int) = cards[i]

    override fun iterator()
            = cards.iterator()
}
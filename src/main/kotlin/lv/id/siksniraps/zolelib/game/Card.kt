package lv.id.siksniraps.zolelib.common

import lv.id.siksniraps.zolelib.moves.Move
import java.util.*

class Card(val rank: Rank, val suit: Suit) : Move {

    init {
        if (rank < Rank.SEVEN || rank < Rank.NINE && suit != Suit.DIAMONDS) {
            throw IllegalStateException("Card not allowed!")
        }
    }

    val points: Int
        get() = when (rank) {
            Rank.SEVEN -> 0
            Rank.EIGHT -> 0
            Rank.NINE -> 0
            Rank.KING -> 4
            Rank.TEN -> 10
            Rank.ACE -> 11
            Rank.JACK -> 2
            Rank.QUEEN -> 3
            else -> throw IllegalStateException("Card not allowed!")
        }

    val trump
        get() = suit == Suit.DIAMONDS || rank in listOf(Rank.QUEEN, Rank.JACK)

    val strength: Int
        get() = when (rank) {
            Rank.SEVEN -> -2
            Rank.EIGHT -> -1
            Rank.NINE -> 0
            Rank.KING -> 1
            Rank.TEN -> 2
            Rank.ACE -> 3
            Rank.JACK -> 10
            Rank.QUEEN -> 14
            else -> throw IllegalStateException("Card not allowed!")
        } + when (suit) {
            Suit.DIAMONDS -> if (rank == Rank.JACK || rank == Rank.QUEEN) 0 else 6
            Suit.HEARTS -> if (rank == Rank.JACK || rank == Rank.QUEEN) 1 else 0
            Suit.SPADES -> if (rank == Rank.JACK || rank == Rank.QUEEN) 2 else 0
            Suit.CLUBS -> if (rank == Rank.JACK || rank == Rank.QUEEN) 3 else 0
        }

    fun copy() = Card(rank, suit)

    override fun toString() = rank.toString() + suit.toString()

    operator fun compareTo(other: Card) = this.strength - other.strength

    override fun equals(other: Any?) = other is Card && suit == other.suit && rank == other.rank

    override fun hashCode(): Int = Objects.hash(rank.ordinal, suit.ordinal)

}
package lv.id.siksniraps.zolelib.moves

enum class Decision : Move {
    TAKE,
    PASS,
    ZOLE,
    SMALL_ZOLE
}
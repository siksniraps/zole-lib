package lv.id.siksniraps

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import lv.id.siksniraps.zolelib.common.Card
import lv.id.siksniraps.zolelib.game.Phase
import lv.id.siksniraps.zolelib.game.Zole
import lv.id.siksniraps.zolelib.moves.Decision
import lv.id.siksniraps.zolelib.players.MonteCarloTreePlayer
import lv.id.siksniraps.zolelib.players.Player

fun main(args: Array<String>) = runBlocking {
    val job = launch(CommonPool) {
        val game = Zole.create(seed = System.currentTimeMillis(),
                players = listOf(
                        MonteCarloTreePlayer("Monte", 2000L),
                        MonteCarloTreePlayer("Carlo", 2000L),
                        ConsolePlayer("Davis")
                )
        )
        game.start()
    }
    job.join()
}

class ConsolePlayer(name: String) : Player(name) {
    override suspend fun doTurn(game: Zole) {

        println("Phase: ${game.phase}")
        println("Cards: ${cards.joinToString(prefix = "[", separator = ", ", postfix = "]") { it.toString() }}")
        println("Moves: ${legalMoves(game).joinToString(prefix = "[", separator = ", ", postfix = "]") { it.toString() }}")
        while (true) {
            val input = readLine()?.toUpperCase()
            if (input != null) {
                when (game.phase) {
                    Phase.DECISION -> if (enumValues<Decision>().any { it.name == input }) return endTurn(Decision.valueOf(input))
                    Phase.BURY, Phase.PLAY -> if (legalMoves(game).any { it.toString() == input }) return endTurn(cards.first { it.toString() == input })
                    else -> return
                }

            }
        }

    }

    override fun onCardPlayed(game: Zole, player: Player, card: Card) {
        if (player != this) {
            println("${player.name} plays: $card")
        }
        super.onCardPlayed(game, player, card)
    }

    override fun onDecisionMade(game: Zole, player: Player, decision: Decision) {
        if (player != this) {
            println("${player.name} decides: $decision")
        }
        super.onDecisionMade(game, player, decision)
    }

    override fun onGameOver(game: Zole) {
        game.players.forEach { println("${it.name}: ${it.score(game)}") }
        super.onGameOver(game)
    }

}